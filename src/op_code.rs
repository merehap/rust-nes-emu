// OpCodes each fall under a specific addressing-mode,
// though a specific OpCode *type* can be present in multiple addressing-modes.
#[derive(Debug)] 
pub enum OpCode {
    Accumulator(Accum),
    Immediate(Imm, u8),
    Absolute(Abs, Address),
    AbsoluteX(AbsX, Address),
    AbsoluteY(AbsY, Address),
    ZeroPage(Zp, Address),
    ZeroPageX(ZpX, Address),
    ZeroPageY(ZpY, Address),
    Implied(Imp),
    Relative(Rel, i8),
    Indirect(Ind, Address),
    // Indexed Indirect
    IndirectX(IndIndex, HalfAddress),
    // Indirect Indexed
    IndirectY(IndIndex, HalfAddress),
}

impl OpCode {
    pub fn byte_length(&self) -> OpCodeLength {
        use op_code::OpCode::*;
        use op_code::OpCodeLength::*;
        match self {
            Accumulator(..) => One,
            Immediate(..)   => Two,
            Absolute(..)    => Three,
            AbsoluteX(..)   => Three,
            AbsoluteY(..)   => Three,
            ZeroPage(..)    => Two,
            ZeroPageX(..)   => Two,
            ZeroPageY(..)   => Two,
            Implied(..)     => One,
            Relative(..)    => Two,
            Indirect(..)    => Three,
            IndirectX(..)   => Two,
            IndirectY(..)   => Two,
        }
    }
}

#[derive(Debug)]
pub enum OpCodeLength {
    One = 1,
    Two,
    Three,
}

#[derive(Debug)] 
pub struct Address {
    value: u16,
}

impl Address {
    fn from_low_high_bytes(low: u8, high: u8) -> Address {
        Address{value: ((u16::from(high)) << 8) | (u16::from(low))}
    }

    fn from_low_byte(low: u8) -> Address {
        Address{value: u16::from(low)}
    }
}

#[derive(Debug)]
pub struct HalfAddress {
    value: u8,
}

impl HalfAddress {
    fn from_u8(value: u8) -> HalfAddress {
        HalfAddress{value}
    }
}

#[derive(Debug)] 
pub enum Accum {
    Asl,
    Rol,
    Lsr,
    Ror,
}

#[derive(Debug)] 
pub enum Imm {
    Ora,
    And,
    Eor,
    Adc,
    Ldy,
    Ldx,
    Lda,
    Cpy,
    Cmp,
    Cpx,
    Sbc,
}

#[derive(Debug)] 
pub enum Abs {
    Ora,
    Asl,
    Jsr,
    Bit,
    And,
    Rol,
    Jmp,
    Eor,
    Lsr,
    Adc,
    Ror,
    Sty,
    Sta,
    Stx,
    Ldy,
    Lda,
    Ldx,
    Cpy,
    Cmp,
    Dec,
    Cpx,
    Sbc,
    Inc,
}

#[derive(Debug)] 
pub enum AbsX {
    Ora,
    Asl,
    And,
    Rol,
    Eor,
    Lsr,
    Adc,
    Ror,
    Sta,
    Ldy,
    Lda,
    Cmp,
    Dec,
    Sbc,
    Inc,
}

#[derive(Debug)] 
pub enum AbsY {
    Ora,
    And,
    Eor,
    Adc,
    Sta,
    Lda,
    Ldx,
    Cmp,
    Sbc,
}

#[derive(Debug)] 
pub enum Zp {
    Ora,
    Asl,
    Bit,
    And,
    Rol,
    Eor,
    Lsr,
    Adc,
    Ror,
    Sty,
    Sta,
    Stx,
    Ldy,
    Lda,
    Ldx,
    Cpy,
    Cmp,
    Dec,
    Cpx,
    Sbc,
    Inc,
}

#[derive(Debug)] 
pub enum ZpX {
    Ora,
    Asl,
    And,
    Rol,
    Eor,
    Lsr,
    Adc,
    Ror,
    Sty,
    Sta,
    Ldy,
    Lda,
    Cmp,
    Dec,
    Sbc,
    Inc,
}

#[derive(Debug)] 
pub enum ZpY {
    Stx,
    Ldx,
}

#[derive(Debug)] 
pub enum Imp {
    Brk,
    Php,
    Clc,
    Plp,
    Sec,
    Rti,
    Pha,
    Cli,
    Rts,
    Pla,
    Sei,
    Dey,
    Txa,
    Tya,
    Txs,
    Tay,
    Tax,
    Clv,
    Tsx,
    Iny,
    Dex,
    Cld,
    Inx,
    Nop,
    Sed,
}

#[derive(Debug)] 
pub enum Rel {
    Bpl,
    Bmi,
    Bvc,
    Bvs,
    Bcc,
    Bcs,
    Bne,
    Beq,
}

#[derive(Debug)] 
pub enum Ind {
    Jmp,
}

#[derive(Debug)] 
pub enum IndIndex {
    Ora,
    And,
    Eor,
    Adc,
    Sta,
    Lda,
    Cmp,
    Sbc,
}

impl OpCode {
    pub fn from_bytes(bytes: [u8; 3]) -> OpCode {
        use op_code::OpCode::*;
        // low - low address byte
        // high - high address byte
        // opr - operand value
        // ofs - offset
        match bytes {
            [0x00,   _,    _] => Implied(Imp::Brk),
            [0x01, low,    _] => IndirectX(IndIndex::Ora, HalfAddress::from_u8(low)),
            [0x05, low,    _] => ZeroPage(Zp::Ora, Address::from_low_byte(low)),
            [0x06, low,    _] => ZeroPage(Zp::Asl, Address::from_low_byte(low)),
            [0x08,   _,    _] => Implied(Imp::Php),
            [0x09, opr,    _] => Immediate(Imm::Ora, opr),
            [0x0A,   _,    _] => Accumulator(Accum::Asl),
            [0x0D, low, high] => Absolute(Abs::Ora, Address::from_low_high_bytes(low, high)),
            [0x0E, low, high] => Absolute(Abs::Asl, Address::from_low_high_bytes(low, high)),

            [0x10, ofs,    _] => Relative(Rel::Bpl, ofs as i8),
            [0x11, low,    _] => IndirectY(IndIndex::Ora, HalfAddress::from_u8(low)),
            [0x15, low,    _] => ZeroPageX(ZpX::Ora, Address::from_low_byte(low)),
            [0x16, low,    _] => ZeroPageX(ZpX::Asl, Address::from_low_byte(low)),
            [0x18,   _,    _] => Implied(Imp::Clc),
            [0x19, low, high] => AbsoluteY(AbsY::Ora, Address::from_low_high_bytes(low, high)),
            [0x1D, low, high] => AbsoluteX(AbsX::Ora, Address::from_low_high_bytes(low, high)),
            [0x1E, low, high] => AbsoluteX(AbsX::Asl, Address::from_low_high_bytes(low, high)),

            [0x20, low, high] => Absolute(Abs::Jsr, Address::from_low_high_bytes(low, high)),
            [0x21, low,    _] => IndirectX(IndIndex::And, HalfAddress::from_u8(low)),
            [0x24, low,    _] => ZeroPage(Zp::Bit, Address::from_low_byte(low)),
            [0x25, low,    _] => ZeroPage(Zp::And, Address::from_low_byte(low)),
            [0x26, low,    _] => ZeroPage(Zp::Rol, Address::from_low_byte(low)),
            [0x28,   _,    _] => Implied(Imp::Plp),
            [0x29, opr,    _] => Immediate(Imm::And, opr),
            [0x2A,   _,    _] => Accumulator(Accum::Rol),
            [0x2C, low, high] => Absolute(Abs::Bit, Address::from_low_high_bytes(low, high)),
            [0x2D, low, high] => Absolute(Abs::And, Address::from_low_high_bytes(low, high)),
            [0x2E, low, high] => Absolute(Abs::Rol, Address::from_low_high_bytes(low, high)),

            [0x30, ofs,    _] => Relative(Rel::Bmi, ofs as i8),
            [0x31, low,    _] => IndirectY(IndIndex::And, HalfAddress::from_u8(low)),
            [0x35, low,    _] => ZeroPageX(ZpX::And, Address::from_low_byte(low)),
            [0x36, low,    _] => ZeroPageX(ZpX::Rol, Address::from_low_byte(low)),
            [0x38,   _,    _] => Implied(Imp::Sec),
            [0x39, low, high] => AbsoluteY(AbsY::And, Address::from_low_high_bytes(low, high)),
            [0x3D, low, high] => AbsoluteX(AbsX::And, Address::from_low_high_bytes(low, high)),
            [0x3E, low, high] => AbsoluteX(AbsX::Rol, Address::from_low_high_bytes(low, high)),

            [0x40,   _,    _] => Implied(Imp::Rti),
            [0x41, low,    _] => IndirectX(IndIndex::Eor, HalfAddress::from_u8(low)),
            [0x45, low,    _] => ZeroPage(Zp::Eor, Address::from_low_byte(low)),
            [0x46, low,    _] => ZeroPage(Zp::Lsr, Address::from_low_byte(low)),
            [0x48,   _,    _] => Implied(Imp::Pha),
            [0x49, opr,    _] => Immediate(Imm::Eor, opr),
            [0x4A,   _,    _] => Accumulator(Accum::Lsr),
            [0x4C, low, high] => Absolute(Abs::Jmp, Address::from_low_high_bytes(low, high)),
            [0x4D, low, high] => Absolute(Abs::Eor, Address::from_low_high_bytes(low, high)),
            [0x4E, low, high] => Absolute(Abs::Lsr, Address::from_low_high_bytes(low, high)),

            [0x50, ofs,    _] => Relative(Rel::Bvc, ofs as i8),
            [0x51, low,    _] => IndirectY(IndIndex::Eor, HalfAddress::from_u8(low)),
            [0x55, low,    _] => ZeroPageX(ZpX::Eor, Address::from_low_byte(low)),
            [0x56, low,    _] => ZeroPageX(ZpX::Lsr, Address::from_low_byte(low)),
            [0x58,    _,   _] => Implied(Imp::Cli),
            [0x59, low, high] => AbsoluteY(AbsY::Eor, Address::from_low_high_bytes(low, high)),
            [0x5D, low, high] => AbsoluteX(AbsX::Eor, Address::from_low_high_bytes(low, high)),
            [0x5E, low, high] => AbsoluteX(AbsX::Lsr, Address::from_low_high_bytes(low, high)),

            [0x60,   _,    _] => Implied(Imp::Rts),
            [0x61, low,    _] => IndirectY(IndIndex::Adc, HalfAddress::from_u8(low)),
            [0x65, low,    _] => ZeroPage(Zp::Adc, Address::from_low_byte(low)),
            [0x66, low,    _] => ZeroPage(Zp::Ror, Address::from_low_byte(low)),
            [0x68,   _,    _] => Implied(Imp::Pla),
            [0x69, opr,    _] => Immediate(Imm::Adc, opr),
            [0x6A,   _,    _] => Accumulator(Accum::Ror),
            [0x6C, low, high] => Indirect(Ind::Jmp, Address::from_low_high_bytes(low, high)),
            [0x6D, low, high] => Absolute(Abs::Adc, Address::from_low_high_bytes(low, high)),
            [0x6E, low, high] => Absolute(Abs::Ror, Address::from_low_high_bytes(low, high)),

            [0x70, ofs,    _] => Relative(Rel::Bvs, ofs as i8),
            [0x71, low,    _] => IndirectY(IndIndex::Adc, HalfAddress::from_u8(low)),
            [0x75, low,    _] => ZeroPageX(ZpX::Adc, Address::from_low_byte(low)),
            [0x76, low,    _] => ZeroPageX(ZpX::Ror, Address::from_low_byte(low)),
            [0x78,   _,    _] => Implied(Imp::Sei),
            [0x79, low, high] => AbsoluteY(AbsY::Adc, Address::from_low_high_bytes(low, high)),
            [0x7D, low, high] => AbsoluteX(AbsX::Adc, Address::from_low_high_bytes(low, high)),
            [0x7E, low, high] => AbsoluteX(AbsX::Ror, Address::from_low_high_bytes(low, high)),

            [0x81, low,    _] => IndirectY(IndIndex::Sta, HalfAddress::from_u8(low)),
            [0x84, low,    _] => ZeroPage(Zp::Sty, Address::from_low_byte(low)),
            [0x85, low,    _] => ZeroPage(Zp::Sta, Address::from_low_byte(low)),
            [0x86, low,    _] => ZeroPage(Zp::Stx, Address::from_low_byte(low)),
            [0x88,   _,    _] => Implied(Imp::Dey),
            [0x8A,   _,    _] => Implied(Imp::Txa),
            [0x8C, low, high] => Absolute(Abs::Sty, Address::from_low_high_bytes(low, high)),
            [0x8D, low, high] => Absolute(Abs::Sta, Address::from_low_high_bytes(low, high)),
            [0x8E, low, high] => Absolute(Abs::Stx, Address::from_low_high_bytes(low, high)),

            [0x90, ofs,    _] => Relative(Rel::Bcc, ofs as i8),
            [0x91, low,    _] => IndirectY(IndIndex::Sta, HalfAddress::from_u8(low)),
            [0x94, low,    _] => ZeroPageX(ZpX::Sty, Address::from_low_byte(low)),
            [0x95, low,    _] => ZeroPageX(ZpX::Sta, Address::from_low_byte(low)),
            [0x96, low,    _] => ZeroPageY(ZpY::Stx, Address::from_low_byte(low)),
            [0x98,   _,    _] => Implied(Imp::Tya),
            [0x99, low, high] => AbsoluteY(AbsY::Sta, Address::from_low_high_bytes(low, high)),
            [0x9A,   _,    _] => Implied(Imp::Txs),
            [0x9D, low, high] => AbsoluteX(AbsX::Sta, Address::from_low_high_bytes(low, high)),

            [0xA0, opr,    _] => Immediate(Imm::Ldy, opr),
            [0xA1, low,    _] => IndirectX(IndIndex::Lda, HalfAddress::from_u8(low)),
            [0xA2, opr,    _] => Immediate(Imm::Ldx, opr),
            [0xA4, low,    _] => ZeroPage(Zp::Ldy, Address::from_low_byte(low)),
            [0xA5, low,    _] => ZeroPage(Zp::Lda, Address::from_low_byte(low)),
            [0xA6, low,    _] => ZeroPage(Zp::Ldx, Address::from_low_byte(low)),
            [0xA8,   _,    _] => Implied(Imp::Tay),
            [0xA9, opr,    _] => Immediate(Imm::Lda, opr),
            [0xAA,   _,    _] => Implied(Imp::Tax),
            [0xAC, low, high] => Absolute(Abs::Ldy, Address::from_low_high_bytes(low, high)),
            [0xAD, low, high] => Absolute(Abs::Lda, Address::from_low_high_bytes(low, high)),
            [0xAE, low, high] => Absolute(Abs::Ldx, Address::from_low_high_bytes(low, high)),

            [0xB0, ofs,    _] => Relative(Rel::Bcs, ofs as i8),
            [0xB1, low,    _] => IndirectY(IndIndex::Lda, HalfAddress::from_u8(low)),
            [0xB4, low,    _] => ZeroPageX(ZpX::Ldy, Address::from_low_byte(low)),
            [0xB5, low,    _] => ZeroPageX(ZpX::Lda, Address::from_low_byte(low)),
            [0xB6, low,    _] => ZeroPageY(ZpY::Ldx, Address::from_low_byte(low)),
            [0xB8,   _,    _] => Implied(Imp::Clv),
            [0xB9, low, high] => AbsoluteY(AbsY::Lda, Address::from_low_high_bytes(low, high)),
            [0xBA,   _,    _] => Implied(Imp::Tsx),
            [0xBC, low, high] => AbsoluteX(AbsX::Ldy, Address::from_low_high_bytes(low, high)),
            [0xBD, low, high] => AbsoluteX(AbsX::Lda, Address::from_low_high_bytes(low, high)),
            [0xBE, low, high] => AbsoluteY(AbsY::Ldx, Address::from_low_high_bytes(low, high)),

            [0xC0, opr,    _] => Immediate(Imm::Cpy, opr),
            [0xC1, low,    _] => IndirectX(IndIndex::Cmp, HalfAddress::from_u8(low)),
            [0xC4, low,    _] => ZeroPage(Zp::Cpy, Address::from_low_byte(low)),
            [0xC5, low,    _] => ZeroPage(Zp::Cmp, Address::from_low_byte(low)),
            [0xC6, low,    _] => ZeroPage(Zp::Dec, Address::from_low_byte(low)),
            [0xC8,   _,    _] => Implied(Imp::Iny),
            [0xC9, opr,    _] => Immediate(Imm::Cmp, opr),
            [0xCA,   _,    _] => Implied(Imp::Dex),
            [0xCC, low, high] => Absolute(Abs::Cpy, Address::from_low_high_bytes(low, high)),
            [0xCD, low, high] => Absolute(Abs::Cmp, Address::from_low_high_bytes(low, high)),
            [0xCE, low, high] => Absolute(Abs::Dec, Address::from_low_high_bytes(low, high)),

            [0xD0, ofs,    _] => Relative(Rel::Bne, ofs as i8),
            [0xD1, low,    _] => IndirectY(IndIndex::Cmp, HalfAddress::from_u8(low)),
            [0xD5, low,    _] => ZeroPageX(ZpX::Cmp, Address::from_low_byte(low)),
            [0xD6, low,    _] => ZeroPageX(ZpX::Dec, Address::from_low_byte(low)),
            [0xD8,   _,    _] => Implied(Imp::Cld),
            [0xD9, low, high] => AbsoluteY(AbsY::Cmp, Address::from_low_high_bytes(low, high)),
            [0xDD, low, high] => AbsoluteX(AbsX::Cmp, Address::from_low_high_bytes(low, high)),
            [0xDE, low, high] => AbsoluteX(AbsX::Dec, Address::from_low_high_bytes(low, high)),

            [0xE0, opr,    _] => Immediate(Imm::Cpx, opr),
            [0xE1, low,    _] => IndirectX(IndIndex::Cmp, HalfAddress::from_u8(low)),
            [0xE4, low,    _] => ZeroPage(Zp::Cpx, Address::from_low_byte(low)),
            [0xE5, low,    _] => ZeroPage(Zp::Sbc, Address::from_low_byte(low)),
            [0xE6, low,    _] => ZeroPage(Zp::Inc, Address::from_low_byte(low)),
            [0xE8,   _,    _] => Implied(Imp::Inx),
            [0xE9, opr,    _] => Immediate(Imm::Sbc, opr),
            [0xEA,   _,    _] => Implied(Imp::Nop),
            [0xEC, low, high] => Absolute(Abs::Cpx, Address::from_low_high_bytes(low, high)),
            [0xED, low, high] => Absolute(Abs::Sbc, Address::from_low_high_bytes(low, high)),
            [0xEE, low, high] => Absolute(Abs::Inc, Address::from_low_high_bytes(low, high)),

            [0xF0, ofs,    _] => Relative(Rel::Beq, ofs as i8),
            [0xF1, low,    _] => IndirectY(IndIndex::Sbc, HalfAddress::from_u8(low)),
            [0xF5, low,    _] => ZeroPageX(ZpX::Sbc, Address::from_low_byte(low)),
            [0xF6, low,    _] => ZeroPageX(ZpX::Inc, Address::from_low_byte(low)),
            [0xF8,   _,    _] => Implied(Imp::Sed),
            [0xF9, low, high] => AbsoluteY(AbsY::Sbc, Address::from_low_high_bytes(low, high)),
            [0xFD, low, high] => AbsoluteX(AbsX::Sbc, Address::from_low_high_bytes(low, high)),
            [0xFE, low, high] => AbsoluteX(AbsX::Inc, Address::from_low_high_bytes(low, high)),

            _ => panic!("Unimplemented OpCode: {:?}", bytes),
        }
    }
}
