struct CPU {
    // The registers.
    accum: u8,
    index_x: u8,
    index_y: u8,
    stack_pointer: u8,
    program_counter: u16,

    status: u8,
    carry_flag: bool,
    zero_flag: bool,
    interrupt_disable: bool,
    break_command: bool,
    overflow_flag: bool,
    negative_flag: bool,
}
