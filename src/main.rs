mod op_code;

use op_code::OpCode;

fn main() {
    let op_code = OpCode::from_bytes([0,0,0]);
    println!("{:?} {:?}", op_code, op_code.byte_length());
}
